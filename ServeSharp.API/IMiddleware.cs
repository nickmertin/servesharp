﻿using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace ServeSharp.API
{
    public interface IMiddleware
    {
        Task<bool> HandleRequest(HttpContext context);
    }
}