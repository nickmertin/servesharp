﻿using System;
using System.Collections.Generic;

namespace ServeSharp.API
{
    public interface IModuleContext : IList<IMiddleware>
    {
        event EventHandler Unloading;
    }
}
