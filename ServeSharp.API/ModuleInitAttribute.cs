﻿using System;

namespace ServeSharp.API
{
    [AttributeUsage(AttributeTargets.Assembly)]
    public sealed class ModuleInitAttribute : Attribute
    {
        public ModuleInitAttribute(Type initHandler, string initMethod = "init")
        {
            InitHandler = initHandler;
            InitMethod = initMethod;
        }

        public Type InitHandler { get; private set; }

        public string InitMethod { get; private set; }
    }
}
