﻿using Microsoft.AspNetCore.Http;
using ServeSharp.API;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace ServeSharp
{
    public static class Config
    {
        private static readonly object _lock = new object();
        private static Module[] _modules = new Module[0];
        private static readonly List<Task> currentRequests = new List<Task>();

        public static void Update(string config)
        {
            var doc = XDocument.Parse(config);
            if (doc.Root.Name != "config")
                throw new Exception("Root element must be named 'config'");
            // Lock to prevent both multiple simultaneous config updates and request resolution during update
            lock (_lock)
            {
                // Wait for all current requests to finish processing
                foreach (var task in currentRequests)
                    task.Wait();
                // Generate new modules array
                var newModules = (from m in from e in doc.Descendants("module") select new ModuleDescriptor(e) select _modules.FirstOrDefault(_m => _m.descriptor == m) ?? new Module(m)).ToArray();
                // Unload old modules
                foreach (var m in _modules)
                    if (!newModules.Any(_m => _m.descriptor == m.descriptor))
                        m.Unload();
                // Load new modules
                foreach (var m in newModules)
                    if (m.domain == null)
                        m.Load();
                // Update array, removing all modules that failed to load
                _modules = (from m in newModules where m.domain != null select m).ToArray();
            }
            Console.WriteLine("Config loaded");
        }

        public static void UnloadAll()
        {
            foreach (var m in _modules)
                m.Unload();
            _modules = new Module[0];
        }

        public static async Task HandleRequest(HttpContext context)
        {
            async Task f()
            {
                Module[] modules;
                lock (_lock)
                    modules = (from m in _modules where context.Request.Path.StartsWithSegments(m.descriptor.prefix) select m).ToArray();
                try
                {
                    foreach (var m in modules)
                        foreach (var handler in m.context)
                            if (await handler.HandleRequest(context))
                                break;
                    Console.WriteLine("Path not handled by ServeSharp modules: " + context.Request.Path);
                    context.Response.StatusCode = 404;
                    await WriteTextResponse(context.Response, "Path not found: " + context.Request.Path);
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Error while handling {context.Request.Path}: {e}");
                    context.Response.StatusCode = 200;
                    await WriteTextResponse(context.Response, "Internal server error");
                }
            }
            Task task;
            lock (_lock)
            {
                task = f();
                currentRequests.Add(task);
            }
            await task;
            lock (currentRequests)
                currentRequests.Remove(task);
        }

        public static async Task WriteTextResponse(HttpResponse response, string message)
        {
            response.Headers["Content-type"] = "text/plain";
            await response.WriteAsync($"{message}\n\n--------\n\nServeSharp ({Assembly.GetExecutingAssembly().GetCustomAttribute<AssemblyFileVersionAttribute>()?.Version ?? "unknown version"})");
        }

        private class ModuleDescriptor : IEquatable<ModuleDescriptor>
        {
            public PathString prefix;
            public string path;
            public DateTime modified;
            public byte[] data;

            public ModuleDescriptor(XElement element)
            {
                prefix = (string)element.Attribute("prefix");
                path = Path.GetFullPath((string)element.Attribute("path"));
                modified = File.GetLastWriteTime(path);
                data = File.ReadAllBytes(path);
            }

            public override bool Equals(object obj) => Equals(obj as ModuleDescriptor);

            public bool Equals(ModuleDescriptor other) => other != null && path == other.path && modified == other.modified;

            public override int GetHashCode() => path.GetHashCode() ^ modified.GetHashCode();

            public static bool operator ==(ModuleDescriptor descriptor1, ModuleDescriptor descriptor2) => EqualityComparer<ModuleDescriptor>.Default.Equals(descriptor1, descriptor2);

            public static bool operator !=(ModuleDescriptor descriptor1, ModuleDescriptor descriptor2) => !(descriptor1 == descriptor2);
        }

        private class Module
        {
            public ModuleDescriptor descriptor;
            public AppDomain domain;
            public ModuleContext context;

            public Module(ModuleDescriptor descriptor)
            {
                this.descriptor = descriptor;
            }

            public void Load()
            {
                try
                {
                    // Create new AppDomain and load the module assembly
                    domain = AppDomain.CreateDomain($"ServeSharp_Model~{descriptor.path}");
                    var assembly = domain.Load(descriptor.data);
                    descriptor.data = null;
                    // Set up the context and call the init method (if specified)
                    context = new ModuleContext();
                    var attr = assembly.GetCustomAttribute<ModuleInitAttribute>();
                    attr?.InitHandler.GetMethod(attr.InitMethod, new[] { typeof(IModuleContext) })?.Invoke(null, new object[] { context });
                }
                catch (Exception e)
                {
                    Console.WriteLine($"Failed to load module '{descriptor.path}': {e}");
                    Unload();
                }
            }

            public void Unload()
            {
                context?.Unload(EventArgs.Empty);
                context = null;
                if (domain != null)
                    AppDomain.Unload(domain);
                domain = null;
            }
        }

        private class ModuleContext : IModuleContext
        {
            private readonly List<API.IMiddleware> pipeline = new List<API.IMiddleware>();

            public API.IMiddleware this[int index]
            {
                get
                {
                    lock (this)
                        return pipeline[index];
                }

                set
                {
                    lock (this)
                        pipeline[index] = value;
                }
            }

            public int Count
            {
                get
                {
                    lock (this)
                        return pipeline.Count;
                }
            }

            public bool IsReadOnly => false;

            public event EventHandler Unloading;

            internal void Unload(EventArgs e)
            {
                Unloading?.Invoke(this, e);
            }

            public void Add(API.IMiddleware item)
            {
                lock (this)
                    pipeline.Add(item);
            }

            public void Clear()
            {
                lock (this)
                    pipeline.Clear();
            }

            public bool Contains(API.IMiddleware item)
            {
                lock (this)
                    return pipeline.Contains(item);
            }

            public void CopyTo(API.IMiddleware[] array, int arrayIndex)
            {
                lock (this)
                    pipeline.CopyTo(array, arrayIndex);
            }

            public IEnumerator<API.IMiddleware> GetEnumerator()
            {
                API.IMiddleware[] copy;
                lock (this)
                {
                    copy = new API.IMiddleware[pipeline.Count];
                    pipeline.CopyTo(copy);
                }
                return copy.AsEnumerable().GetEnumerator();
            }

            IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

            public int IndexOf(API.IMiddleware item)
            {
                lock (this)
                    return pipeline.IndexOf(item);
            }

            public void Insert(int index, API.IMiddleware item)
            {
                lock (this)
                    pipeline.Insert(index, item);
            }

            public bool Remove(API.IMiddleware item)
            {
                lock (this)
                    return pipeline.Remove(item);
            }

            public void RemoveAt(int index)
            {
                lock (this)
                    pipeline.RemoveAt(index);
            }
        }
    }
}
