﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Mono.Options;
using System;
using System.IO;

namespace ServeSharp
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var showHelp = false;
            string configPath = null;
            var options = new OptionSet
            {
                { "h|help", "show this message and exit", h => showHelp = h != null },
                { "c|config=", "load configuration file", c => configPath = c }
            };

            try
            {
                var others = options.Parse(args);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return;
            }

            if (showHelp)
            {
                options.WriteOptionDescriptions(Console.Out);
                return;
            }

            try
            {
                Config.Update(configPath == null ? Console.In.ReadToEnd() : File.ReadAllText(configPath));
            }
            catch (Exception e)
            {
                Console.WriteLine("Failed to read configuration: " + e.Message);
                return;
            }

            BuildWebHost(args).Run();
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .Build();
    }
}
